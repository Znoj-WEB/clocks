### **Description**

Game Clocks - Let's allocate same time for multiple clocks and switch between them in the same way as with the "chess clocks". Initial time can be set as well as number of clocks.

---

### **Link**

served with Firebase:
[https://clocks.znoj.cz/](https://clocks.znoj.cz/)

---

### **Technology**

TypeScript, Ant Design, React, firebase, GitLab CI/CD

---

### **Year**

2023

---

### **Screenshots**

![](./README/main.png)

![](./README/1.png)
